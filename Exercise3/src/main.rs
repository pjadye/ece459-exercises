use std::thread;

static N: i32 = 10;  // number of threads to spawn

// You should modify main() to spawn multiple threads and join() them
fn main() {
    let mut children = vec![];

    for i in 0..N {
        children.push(thread::spawn(move || {println!("Thread {}", i)}));
    }

    for t in children {
        t.join().unwrap();
    }
}
