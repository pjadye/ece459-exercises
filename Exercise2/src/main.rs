// You should implement the following function
fn fibonacci_number(n: u32) -> u32 {
    let mut num1 = 0;
    let mut num2 = 1;

    for _ in 1..n {
        let temp = num2;
        num2 = num1 + num2;
        num1 = temp;
    }

    num2
}


fn main() {
    println!("{}", fibonacci_number(10));
}
