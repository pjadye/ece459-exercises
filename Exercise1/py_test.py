number = 1000
multiple1 = 5
multiple2 = 3

x = multiple1
y = multiple2

sum = 0

while x < number:
    sum += x
    x += multiple1

while y < number:
    sum += y
    y += multiple2

print(sum)
