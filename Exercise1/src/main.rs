// You should implement the following function:

fn sum_of_multiples(mut number: i32, multiple1: i32, multiple2: i32) -> i32
{
    let mut x = multiple1;
    let mut y = multiple2;
    let mut sum = 0;

    while x < number {
        sum += x;
        x += multiple1;
    }

    while y < number {
        if y % multiple1 != 0 {
            sum += y;
        }
        y += multiple2;
    }

    sum
}

fn main() {
    println!("{}", sum_of_multiples(1000, 5, 3));
}
