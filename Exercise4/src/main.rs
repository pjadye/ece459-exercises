use std::sync::mpsc;
use std::thread;
use std::time::Duration;

// You should modify main() to spawn threads and communicate using channels
fn main() {
    let (tx, rx) = mpsc::channel();

    // let tx1 = mpsc::Sender::clone(&tx);
    let mut threads = vec![];

    for i in 0..3 {
        let tx = mpsc::Sender::clone(&tx);
        threads.push(
            thread::spawn(move || {
                tx.send(format!("Hello from thread {}", i)).unwrap();
            }));
    }

    for t in threads {
        t.join().unwrap();
    }

    drop(tx);

    for received in rx {
        println!("Got: {}", received);
        thread::sleep(Duration::from_secs(1));
    }
}
